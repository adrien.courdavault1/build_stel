How to use:


1. install docker on main machine
2. add user to docker group: `sudo usermod -a -G docker $USER`
3. setup and run jenkins with jenkins_config.sh
4. apparently still need in this machine to change `chmod 666 /var/default/docker.sock` => to check, or include in first image
5. NOW we can use the jenkins pipeline jobs

Setup job
1. create pipeline job
2. pipeline from SCM
3. this job build a machine